# TrackPong - tournament app
The aim of this project is to create an application that wil be used for creating table tennis tournaments.
This application gets rid of the hassle of keeping track of a table tennis tournament manually.
The reason we wanted to create this application was in accordance to the project assignment.

##Technologies
The project is created with Java 17.

##Setup
The application will be wrapped up as a zip-file and includes the executable
.jar file which itself contains everything a computer would need to run the
application. The zip-file also includes the README.md file and the user
manual. The zip-file will be found in the GIT repository.

##Features
* Create tournaments with either 4, 8 or 16 players.
* Create players for the tournament after choosing format.
* Play a single round friendly match.
* View info about the application.
* View the rules of table tennis on an external website.
* Save a group of players.
* Load a chosen group of players.


