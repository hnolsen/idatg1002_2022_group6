package no.ntnu.idatg1002.idatg1002_2022_group6.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This is a tester class for the PlayerRegister class.
 *
 * @author Group 6
 * @version 1.4.2
 */
class PlayerRegisterTest {

  private List<Player>   listOfPlayers;
  private PlayerRegister playerRegister;
  private Player player1;
  private Player player2;
  private Player player3;
  private Player player4;
  private Player player5;
  private Player player6;
  private Player player7;
  private Player player8;
  private Player player9;
  private Player player10;
  private Player player11;
  private Player player12;
  private Player player13;
  private Player player14;
  private Player player15;
  private Player player16;

  /**
   * This method is executed before each test.
   */
  @BeforeEach
  void setUp() {
    playerRegister = new PlayerRegister();
    player1 = new Player("Player One");
    player2 = new Player("Player Two");
    player3 = new Player("Player Three");
    player4 = new Player("Player Four");
    player5 = new Player("Player Five");
    player6 = new Player("Player Six");
    player7 = new Player("Player Seven");
    player8 = new Player("Player Eight");
    player9 = new Player("Player Nine");
    player10 = new Player("Player Ten");
    player11 = new Player("Player Eleven");
    player12 = new Player("Player Twelve");
    player13 = new Player("Player Thirteen");
    player14 = new Player("Player Fourteen");
    player15 = new Player("Player Fifteen");
    player16 = new Player("Player Sixteen");

    listOfPlayers = List.of(player1, player2, player3, player4, player5, player6, player7, player8,
        player9, player10, player11, player12, player13, player14, player15, player16);
  }

  /**
   * Tests the getParticipants method. Should return an empty list when there are no players in the
   * participants list.
   */
  @Test
  void getParticipantsTest_ReturnsEmptyList() {
    assertEquals(Collections.emptyList(), playerRegister.getParticipants());
  }

  /**
   * Tests the getParticipants method. Should return the list of players in the participants list,
   * when there are players in the participants list.
   */
  @Test
  void getParticipantsTest_ReturnsListWithOnePlayer() {
    playerRegister.addPlayers(listOfPlayers);
    assertEquals(listOfPlayers, playerRegister.getParticipants());
  }

  /**
   * Test the getPlayer() method. Should return the player at the given index.
   */
  @Test
  void getPlayer_ReturnsPlayerAtIndex() {
    playerRegister.addPlayer(player1);
    assertEquals(player1, playerRegister.getPlayer(0));
  }

  /**
   * Test the getPlayer() method. Should not return the wrong player at a given index.
   */
  @Test
  void getPlayer_DoesntReturnWrongPlayerAtIndex() {
    playerRegister.addPlayer(player1);
    playerRegister.addPlayer(player2);
    assertNotEquals(player1, playerRegister.getPlayer(1));
  }

  /**
   * Test the getPlayer() method. Should return null when no player was found at that index.
   */
  @Test
  void getPlayer_ReturnsNullWhenNoPlayerFound() {
    assertNull(playerRegister.getPlayer(0));
  }

  /**
   * Test the getPlayer() method. Should return the player by the given name.
   */
  @Test
  void getPlayer_ReturnsPlayerGivenName() {
    playerRegister.addPlayers(listOfPlayers);
    assertEquals(player1, playerRegister.getPlayer("Player One"));
  }

  /**
   * Test the getPlayer() method. Should return null of no player was found by the given name.
   */
  @Test
  void getPlayer_ReturnsNullWhenNoPlayerFoundGivenName() {
    assertNull(playerRegister.getPlayer("Player One"));
  }

  @Test
  void addPlayer() {
    playerRegister.addPlayer(player1);
    assertEquals(playerRegister.getPlayer("Player One"), player1);
  }

  @Test
  void addPlayers() {
    playerRegister.addPlayers(listOfPlayers);
    assertEquals(listOfPlayers, playerRegister.getParticipants());
  }

  @Test
  void removePlayer() {
    playerRegister.addPlayers(listOfPlayers);
    playerRegister.removePlayer(player1);
    try {
      assertNotEquals(playerRegister.getParticipants(), listOfPlayers);
    }catch (Exception e){
      System.err.println(e);
      fail(e);
    }
  }

  //good practice is lættis
  /*
  @Test
  void getRandomPlayers() {

  }

   */

  @Test
  void clear() {
    playerRegister.addPlayers(listOfPlayers);
    playerRegister.clear();
    assertEquals("[]", playerRegister.getParticipants().toString());
  }
}