package no.ntnu.idatg1002.idatg1002_2022_group6.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This is a tester class for the Player class. It should test most of the functionality.
 *
 * @author Group 6
 * @version 1.1.2
 */
class PlayerTest {

  private Player player1;
  private Player player2;
  private Player player3;
  private Player player4;
  private final List<String>
      invalidNames = List.of(" leading", "trailing ", " leadTrail ", "1", "234324", "¤%&/(%)=");

  /**
   * This method is executed before each test. It instantiates new players.
   */
  @BeforeEach
  void setUp() {
    player1 = new Player("Player One");
    player2 = new Player("Player Two");
    player3 = new Player("Player Three");
    player4 = new Player("Player Four");
  }

  /**
   * Tests that the Player() constructor throws an IllegalArgumentException when the name is blank.
   */
  @Test
  void playerConstructorTest_ThrowsIllegalArgumentExceptionWhenNameIsEmpty() {
    assertThrows(IllegalArgumentException.class, () -> new Player(""));
  }

  /**
   * Tests that the Player() constructor throws an IllegalArgumentException when the name
   * is invalid.
   */
  @Test
  void playerConstructorTest_ThrowsIllegalArgumentExceptionWhenNameIsNull() {
    for(String name : invalidNames) {
      assertThrows(IllegalArgumentException.class, () -> new Player(name));
    }
  }

  /**
   * Tests that the Player() constructor returns the correct exception message when an exception
   * is thrown.
   */
  @Test
  void playerConstructorTest_CorrectExceptionMessage() {
    try {
      new Player("");
    } catch (IllegalArgumentException e) {
      assertEquals("The name cannot contain any leading, trailing or double whitespaces." +
          "It should be between 2 and 15 characters long, and not contain any special " +
          "characters beside a dash or a hyphen.", e.getMessage());
    }
  }

  /**
   * Tests that the getName() method returns the correct name.
   */
  @Test
  void getNameTest_GetsCorrectName() {
    assertEquals("Player One", player1.getName());
  }

  /**
   * Tests that the getName() method doesn't return an incorrect name.
   */
  @Test
  void getNameTest_DoesntGetIncorrectName() {
    for (String name : invalidNames) {
      assertNotEquals(name, player1.getName());
    }
  }

  /**
   * Tests that the toString method returns the correct string.
   */
  @Test
  void playerToStringTest_ReturnsCorrectString() {
    assertEquals("Player One", player1.toString());
  }

  /**
   * Tests that the overridden equals() method works by comparing the same object to itself, and
   * asserting that they are equal.
   */
  @Test
  void equalsTest_Player1EqualsPlayer1() {
    assertEquals(player1, player1);
  }

  /**
   * Tests that the overridden equals() method works by comparing two different objects, and
   * asserting that they are not equal.
   */
  @Test
  void equalsTest_Player1DoesntEqualPlayer2() {
    assertNotEquals(player3, player4);
    assertNotEquals(null, player1);
    assertNotEquals(new Object(), player2);
  }

  /**
   * Tests that the equals() method will return false if a null object is compared to it, or if
   * an object of a different class is compared to it. SonarLint does not like this, but how else
   * is this particular test possible?
   * TODO find a way to test this that SonarLint doesn't complain about.
   */
  @Test
  void equalsTest_ReturnsFalseForNull() {
    assertFalse(new Player("fdsa").equals(null));
    assertFalse(player1.equals("fdsa"));
  }

  /**
   * Test that the hashcode() method returns the correct hashcode.
   */
  @Test
  void testHashCode_ReturnsCorrectHashCode() {
    assertEquals(player1.hashCode(), player1.hashCode());
    assertNotEquals(player1.hashCode(), player2.hashCode());
  }
}