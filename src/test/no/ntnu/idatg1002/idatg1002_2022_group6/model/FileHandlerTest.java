package no.ntnu.idatg1002.idatg1002_2022_group6.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlerTest {
    private Player player1;
    private Player player2;
    private Player player3;
    private Player player4;
    private PlayerRegister playerRegister;
    private PlayerRegister secondRegister;


    @BeforeEach
    void setUp() {
        player1 = new Player("Player One");
        player2 = new Player("Player Two");
        player3 = new Player("Player Three");
        player4 = new Player("Player Four");
        playerRegister = new PlayerRegister();
        secondRegister = new PlayerRegister();
        playerRegister.addPlayer(player1); playerRegister.addPlayer(player2);
        playerRegister.addPlayer(player3); playerRegister.addPlayer(player4);
    }

    @Test
    void writeCsv() {
        try {
            FileHandler.writeCsv(playerRegister.getParticipants(), "TestFile");
            secondRegister.addPlayers(FileHandler.readCsv(Path.of("TestFile")));
            assertEquals(playerRegister.getParticipants().toString(), secondRegister.getParticipants().toString());
        }catch (Exception e){
            System.err.println(e);
            fail();
        }
    }

    @Test
    void readCsv() {
        secondRegister.addPlayers(FileHandler.readCsv(Path.of("TestFile")));
        assertEquals(playerRegister.getParticipants().toString(), secondRegister.getParticipants().toString());
    }
}