module no.ntnu.idatg1002.idatg1002_2022_group6 {
  requires javafx.controls;
  requires javafx.fxml;
    requires java.datatransfer;
  requires java.desktop;


  opens no.ntnu.idatg1002.idatg1002_2022_group6 to javafx.fxml;
  exports no.ntnu.idatg1002.idatg1002_2022_group6;
    exports no.ntnu.idatg1002.idatg1002_2022_group6.model;
    opens no.ntnu.idatg1002.idatg1002_2022_group6.model to javafx.fxml;
}