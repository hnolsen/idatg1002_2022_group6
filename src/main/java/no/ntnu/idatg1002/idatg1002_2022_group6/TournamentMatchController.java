package no.ntnu.idatg1002.idatg1002_2022_group6;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.FileHandler;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.Match;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.Player;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Represents the tournament match controller.
 *
 * @author Group6
 * @version 1.0
 */
public class TournamentMatchController extends TournamentController implements Initializable {
  private Player player1;
  private Player player2;

  @FXML
  private Label  matchPlayer1label;
  @FXML
  private Label  matchPlayer2label;
  @FXML
  private Label  scorePlayer1;
  @FXML
  private Label  scorePlayer2;
  @FXML
  private Label  winnerLabel;
  @FXML
  private Label  infoLabel;
  @FXML
  private Button pointPlayer1;
  @FXML
  private Button pointPlayer2;
  @FXML
  private Button nextMatch;

  private final Match match = new Match();

  /**
   * Initializes the match, retrieves the players from the register
   * Also removes them from the register, later to be added if one wins, more logical
   *
   * @param url            url
   * @param resourceBundle ResourceBundle
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    nextMatch.setOpacity(0);
    nextMatch.setDisable(true);
    if (TournamentController.tournamentRegister.getParticipants().size() >= 2) {
      player1 = TournamentController.tournamentRegister.getPlayer(0);
      player2 = TournamentController.tournamentRegister.getPlayer(1);
      matchPlayer1label.setText(
          player1.toString());
      matchPlayer2label.setText(
          player2.toString());
    }
    else {
      winnerLabel.setText(
          "The winner is: " + TournamentController.tournamentRegister.getPlayer(0).toString());
      pointPlayer1.setDisable(true);
      pointPlayer2.setDisable(true);
      scorePlayer1.setOpacity(0);
      scorePlayer2.setOpacity(0);
    }
  }

  /**
   * Adds a point to player 1, stops displaying the button when the score reaches 12
   * Adds player 1 as the winner back to the registry (at the end)
   */
  public void onPressPointPlayer1() {
    match.scorePlayer1();
    scorePlayer1.setText(String.valueOf(Integer.parseInt((scorePlayer1.getText())) + 1));
    try {
      if (Integer.parseInt(scorePlayer1.getText()) >= 11) {
        TournamentController.tournamentRegister.removePlayer(player1);
        TournamentController.tournamentRegister.removePlayer(player2);
        pointPlayer1.setDisable(true);
        pointPlayer2.setDisable(true);
        winnerLabel.setText(
            "Winner: " + player1.toString());
        infoLabel.setText("Press next match to continue the tournament");
        Player winner = player1;
        TournamentController.tournamentRegister.addPlayer(winner);
        nextMatch.setOpacity(1);
        nextMatch.setDisable(false);
      }
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  /**
   * Adds a point to player 2, stops displaying the button when the score reaches 12
   * Adds player 1 as the winner back to the registry (at the end)
   */
  public void onPressPointPlayer2() {
    match.scorePLayer2();
    scorePlayer2.setText(String.valueOf(Integer.parseInt((scorePlayer2.getText())) + 1));
    if (Integer.parseInt(scorePlayer2.getText()) >= 11) {
      TournamentController.tournamentRegister.removePlayer(player1);
      TournamentController.tournamentRegister.removePlayer(player2);
      pointPlayer1.setDisable(true);
      pointPlayer2.setDisable(true);
      winnerLabel.setText(
          "Winner: " + player2.toString());
      infoLabel.setText("Press next match to continue the tournament");
      Player winner = player2;
      TournamentController.tournamentRegister.addPlayer(winner);
      nextMatch.setOpacity(1);
      nextMatch.setDisable(false);
    }
  }

  /**
   * Proceeds to the next match, reloads the same window but with the new players
   *
   * @throws IOException If the fxml file is not found
   */
  public void onNextMatchPress() throws IOException {
    TableTennisApplication.changeScene("tournament-match-view.fxml");
  }

  /**
   * Returns to the main menu
   *
   * @throws IOException if the fxml file is not found
   */
  @Override
  public void onPressReturnToMenu() throws IOException {
    TableTennisApplication.changeScene("table-tennis-view.fxml");
  }

  /**
   * Saves the match to a csv file for later use
   *
   * @throws IOException if the file is not found
   */
  public void onPressSaveMatch() throws IOException {
    FileHandler.writeCsv(TournamentController.tournamentRegister.getParticipants(),
        "NEWTOURNAMENT");
    TournamentController.tournamentRegister.clear();
    TableTennisApplication.changeScene("table-tennis-view.fxml");
  }


}
