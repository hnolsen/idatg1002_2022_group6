package no.ntnu.idatg1002.idatg1002_2022_group6;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.FileHandler;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.Player;

import java.io.IOException;
import java.nio.file.Path;



/**
 * Controller for the Bracket View GUI. Handles all user interaction with the GUI.
 *
 * @author Group6
 * @version 1.0
 */

public class BracketViewController32 extends TournamentController {


    @FXML
    private TextField textFieldPlayer1;
    @FXML
    private TextField  textFieldPlayer2;
    @FXML
    private TextField textFieldPlayer3;
    @FXML
    private TextField textFieldPlayer4;
    @FXML
    private TextField textFieldPlayer5;
    @FXML
    private TextField textFieldPlayer6;
    @FXML
    private TextField textFieldPlayer7;
    @FXML
    private TextField textFieldPlayer8;
    @FXML
    private TextField textFieldPlayer9;
    @FXML
    private TextField textFieldPlayer10;
    @FXML
    private TextField textFieldPlayer11;
    @FXML
    private TextField textFieldPlayer12;
    @FXML
    private TextField textFieldPlayer13;
    @FXML
    private TextField textFieldPlayer14;
    @FXML
    private TextField textFieldPlayer15;
    @FXML
    private TextField textFieldPlayer16;
    @FXML
    private TextField textFieldPlayer17;
    @FXML
    private TextField textFieldPlayer18;
    @FXML
    private TextField textFieldPlayer19;
    @FXML
    private TextField textFieldPlayer20;
    @FXML
    private TextField textFieldPlayer21;
    @FXML
    private TextField textFieldPlayer22;
    @FXML
    private TextField textFieldPlayer23;
    @FXML
    private TextField textFieldPlayer24;
    @FXML
    private TextField textFieldPlayer25;
    @FXML
    private TextField textFieldPlayer26;
    @FXML
    private TextField textFieldPlayer27;
    @FXML
    private TextField textFieldPlayer28;
    @FXML
    private TextField textFieldPlayer29;
    @FXML
    private TextField textFieldPlayer30;
    @FXML
    private TextField textFieldPlayer31;
    @FXML
    private TextField textFieldPlayer32;

    /**
     * button to go back to the previous scene
     * @throws IOException IOException
     */
    public void onPressBackToTournamentView() throws IOException {
        TableTennisApplication tableTennisApplication = new TableTennisApplication();
        tableTennisApplication.changeScene("tournament-view.fxml");
    }

    /**
     * Adds players with the names given in the textfields to a register handled in the BracketviewController
     * Also clears the register before it adds as an extra security step
     */
    public void onPressAdd() {
        if(textFieldPlayer1.getText().isBlank() || textFieldPlayer2.getText().isBlank()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Please enter a name into the text field before proceeding!");
            alert.setTitle("WARNING");
            alert.showAndWait();
        } else {
            try {
                BracketViewController.tournamentRegister.clear();

                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer1.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer2.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer3.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer4.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer5.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer6.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer7.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer8.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer9.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer10.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer11.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer12.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer13.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer14.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer15.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer16.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer17.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer18.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer19.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer20.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer21.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer22.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer23.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer24.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer25.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer26.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer27.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer28.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer29.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer30.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer31.getText()));
                BracketViewController.tournamentRegister.addPlayer(new Player(textFieldPlayer32.getText()));
            }
            catch (Exception e)
            {
                System.err.println(e);
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("You need to use letters and only letters!");
                alert.setTitle("UNABLE TO CREATE BRACKET");
                alert.showAndWait();
            }
        }
    }

    /**
     * Loads from the NEWTOURNAMENT.csv file, proceeds to the first game
     */
    public void onPressLoad() {
        try
        {
            BracketViewController.tournamentRegister.clear();
            BracketViewController.tournamentRegister.addPlayers(FileHandler.readCsv(Path.of("NEWTOURNAMENT.csv")));
            TableTennisApplication tableTennisApplication = new TableTennisApplication();
            tableTennisApplication.changeScene("tournament-match-view.fxml");

        }catch (Exception e){
                System.err.println(e);
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("No save found!");
                alert.setTitle("UNABLE TO CREATE BRACKET");
                alert.showAndWait();
        }
    }

    /**
     * Proceeds to the match
     * @throws IOException
     */
    public void onPressPlay() throws IOException{
        TableTennisApplication tableTennisApplication = new TableTennisApplication();
        tableTennisApplication.changeScene("create-tournament-match.fxml");
    }
}