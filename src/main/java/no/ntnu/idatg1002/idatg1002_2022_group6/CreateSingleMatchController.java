package no.ntnu.idatg1002.idatg1002_2022_group6;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Represents a controller for the "Create bracket" stage.
 *
 * @author Group6
 * @version 1.0
 */
public class CreateSingleMatchController extends TournamentController implements Initializable {
  @FXML
  private Label player1Label;
  @FXML
  private Label player2Label;


  /**
   * Initializes the match, gets names from the register
   * @param url
   * @param resourceBundle
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    player1Label.setText(TournamentController.tournamentRegister.getPlayer(0).toString());
    player2Label.setText(TournamentController.tournamentRegister.getPlayer(1).toString());
  }

  /**
   * Switches scene to the match screen
   * @throws IOException
   */
  public void onPressPlayMatch() throws IOException {
    TableTennisApplication tableTennisApplication = new TableTennisApplication();
    tableTennisApplication.changeScene("match-view.fxml");
  }

  /**
   * Returns to the tournament view
   * @throws IOException
   */
  public void onPressBackInSingleMatchView() throws IOException {
    TableTennisApplication tableTennisApplication = new TableTennisApplication();
    tableTennisApplication.changeScene("table-tennis-view.fxml");
  }
}
