package no.ntnu.idatg1002.idatg1002_2022_group6;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.FileHandler;

/**
 * Controller for the Table Tennis GUI. Handles all user interaction with the GUI.
 *
 * @author Group6
 * @version 1.0
 */
public class TableTennisController implements Initializable {

  @FXML
  private ImageView mainMenuLogo;

  /**
   *  Initializes the controller class after the fxml file has been loaded.
   * @param url the location used to resolve relative paths for the root object, or null if the
   *            location is not known.
   * @param resourceBundle the resources used to localize the root object, or null if the root
   *                       object was not localized.
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    File file = new File("src/main/resources/no/ntnu/idatg1002/idatg1002_2022_group6/graphics/TrackPongMainLogo.png" );
    Image mainLogo = new Image(file.toURI().toString());
    mainMenuLogo.setImage(mainLogo);
  }

  /**
   * Changes the scene to the tournament view.
   * @throws IOException if the file cannot be found.
   */
  public void onClickStartTournament() throws IOException {
    TableTennisApplication.changeScene("tournament-view.fxml");
  }

  /**
   * Opens the information page in a new scene.
   * @throws IOException if the file cannot be found.
   */
  public void onClickShowInfo() throws IOException {
    TableTennisApplication.changeScene("info-menu-view.fxml");
  }

  /**
   * When the "Show Rules" button is clicked, an alert with a confirmation button will be shown,
   * asking the user to agree to be taken to an external website.
   */
  public void onClickShowRules() {
    URI pongFitURI = URI.create("https://www.pongfit.org/official-rules-of-table-tennis/");
    try {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
          "An external web page will now be opened. This will minimize the application. Continue?");
      alert.setTitle("Confirmation");
      alert.setContentText("This will open an external tab and leave the application. Continue?");
      Optional<ButtonType> choice = alert.showAndWait();
      if (choice.isPresent() && choice.get() == ButtonType.OK) {
        Desktop.getDesktop()
            .browse(new URI(pongFitURI.toString()));
      }
    } catch (IOException | URISyntaxException e) {
      e.printStackTrace();
    }
  }

  /**
   * Changes the scene to the single match view.
   * @throws IOException if the file cannot be found.
   */
  public void onClickFriendlyMatch() throws IOException {
    TableTennisApplication.changeScene("single-match-view.fxml");
  }

  /**
   * Loads in the most recently saved game
   */
  public void onLoadPressed(){
    try
    {
      TournamentController.tournamentRegister.addPlayers(FileHandler.readCsv(Path.of("SINGLELOAD.csv")));
      TableTennisApplication.changeScene("create_single_match_view.fxml");

    }catch (Exception e){
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setContentText("No save found!");
      alert.setTitle("UNABLE TO CREATE BRACKET");
      alert.showAndWait();
    }
  }

  /**
   * Exits the application.
   */
  public void onClickExitApp() {
    System.exit(0);
  }

}