package no.ntnu.idatg1002.idatg1002_2022_group6;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.FileHandler;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.Match;

/**
 * Represents the match controller. This class is responsible for handling the match view, and
 * displaying information about an ongoing game.
 *
 * @author Group6
 * @version 1.0
 */
public class MatchController extends TournamentController implements Initializable {
  @FXML
  private Label  matchPlayer1label;
  @FXML
  private Label  matchPlayer2label;
  @FXML
  private Label  scorePlayer1;
  @FXML
  private Label  scorePlayer2;
  @FXML
  private Label  winnerLabel;
  @FXML
  private Label  infoLabel;
  @FXML
  private Button pointPlayer1;
  @FXML
  private Button pointPlayer2;

  private final Match match = new Match();

  /**
   * Initializes the match, retrieves the players from the register
   * @param url url
   * @param resourceBundle ResourceBundle
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    matchPlayer1label.setText(
        TournamentController.tournamentRegister.getPlayer(0).toString());
    matchPlayer2label.setText(
        TournamentController.tournamentRegister.getPlayer(1).toString());
  }

  /**
   * Adds a point to player 1. When any player has a score equal to 11, the game is over.
   * As a visual feedback the button is disabled.
   */
  public void onPressPointPlayer1() {
    match.scorePlayer1();
    scorePlayer1.setText(String.valueOf(Integer.parseInt((scorePlayer1.getText())) + 1));
    if (Integer.parseInt(scorePlayer1.getText()) >= 11) {
      pointPlayer1.setDisable(true);
      pointPlayer2.setDisable(true);
      winnerLabel.setText(
          "Winner: " + TournamentController.tournamentRegister.getPlayer(0).toString());
      infoLabel.setText("Return to main menu to start a new match");
    }
  }

  /**
   * Adds a point to player 2. When any player has a score equal to 11, the game is over.
   * As a visual feedback the button is disabled.
   */
  public void onPressPointPlayer2() {
    match.scorePLayer2();
    scorePlayer2.setText(String.valueOf(Integer.parseInt((scorePlayer2.getText())) + 1));
    if (Integer.parseInt(scorePlayer2.getText()) >= 11) {
      pointPlayer1.setDisable(true);
      pointPlayer2.setDisable(true);
      winnerLabel.setText(
          "Winner: " + TournamentController.tournamentRegister.getPlayer(1).toString());
      infoLabel.setText("Exit to main menu to start new match");
    }
  }

  /**
   * Returns to the main menu
   *
   * @throws IOException IOException
   */
  @Override
  public void onPressReturnToMenu() throws IOException {
    TableTennisApplication.changeScene("table-tennis-view.fxml");
  }

  /**
   * When the save match button is pressed, the match is saved to the file.
   *
   * @throws IOException If the file cannot be saved.
   */
  public void onPressSaveMatch() throws IOException{
    FileHandler.writeCsv(tournamentRegister.getParticipants(), "SINGLELOAD");
    TournamentController.tournamentRegister.clear();
    TableTennisApplication.changeScene("table-tennis-view.fxml");
  }
}
