package no.ntnu.idatg1002.idatg1002_2022_group6;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.Player;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.PlayerRegister;

/**
 * Represents the tournament controller. This class is responsible for controlling the tournament
 * view. It handles the input from the user to the text fields that will register players. Displays
 * error messages when the user enters invalid input.
 *
 * @author Group6
 * @version 1.0
 */
public class TournamentController {
  public static final PlayerRegister tournamentRegister = new PlayerRegister();
  @FXML
  private             Label          playersAddedLabel;
  @FXML
  private             TextField      textField1;
  @FXML
  private             TextField      textField2;
  @FXML
  private             Button         createBracket;

  /**
   * Method adds player's names to tournamentRegister that are written into fields by a user.
   * If no name is written, an alert is shown.
   */
  public void onPressAddPlayers() {
    if (textField1.getText().isBlank() || textField2.getText().isBlank()) {
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setContentText("Please enter a name into the text field before proceeding!");
      alert.setTitle("WARNING");
      alert.showAndWait();
    }
    else {
      try {
        tournamentRegister.clear();
        tournamentRegister.addPlayer(new Player(textField1.getText().strip()));
        tournamentRegister.addPlayer(new Player(textField2.getText().strip()));
        playersAddedLabel.setText("Players added!");
        createBracket.setOpacity(1);
      } catch (Exception e) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setContentText("You need to use letters and only letters!");
        alert.setTitle("UNABLE TO CREATE BRACKET");
        alert.showAndWait();
      }
    }
  }

  /**
   * Button to create a bracket after entering players
   *
   * @throws IOException If the file is not found.
   */
  public void onClickCreateBracket() throws IOException {
    TableTennisApplication.changeScene("create_single_match_view.fxml");
  }

  /**
   * Returns the user to the main menu scene when the back button is pressed.
   *
   * @throws IOException If the file is not found.
   */
  public void onPressReturnToMenu() throws IOException {
    TableTennisApplication.changeScene("table-tennis-view.fxml");
  }

  /**
   * Creates a 4 player tournament.
   *
   * @throws IOException if the file is not found
   */
  public void create4PlayerTournament() throws IOException {
    TableTennisApplication.changeScene("create-bracket-view4.fxml");
  }

  /**
   * Creates an 8 player tournament
   *
   * @throws IOException if the file is not found
   */
  public void create8PlayerTournament() throws IOException {
    TableTennisApplication.changeScene("create-bracket-view8.fxml");
  }

  /**
   * Creates a 16 player tournament.
   *
   * @throws IOException if the file is not found
   */
  public void create16PlayerTournament() throws IOException {
    TableTennisApplication.changeScene("create-bracket-view.fxml");
  }

  /**
   * Creates a 32 player tournament
   *
   * @throws IOException if the file is not found.
   */
  public void create32PlayerTournament() throws IOException {
    TableTennisApplication.changeScene("create-bracket-view32.fxml");
  }


}
