package no.ntnu.idatg1002.idatg1002_2022_group6;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.FileHandler;
import no.ntnu.idatg1002.idatg1002_2022_group6.model.Player;

import java.io.IOException;
import java.nio.file.Path;


/**
 * Controller for the Bracket View GUI. Handles all user interaction with the GUI.
 *
 * @author Group6
 * @version 1.0
 */

public class BracketViewController4 extends TournamentController {

  @FXML
  private Label     playersAddedMessage;
  @FXML
  private TextField textFieldPlayer1;
  @FXML
  private TextField textFieldPlayer2;
  @FXML
  private TextField textFieldPlayer3;
  @FXML
  private TextField textFieldPlayer4;


  /**
   * button to go back to the previous scene
   *
   * @throws IOException IOException
   */
  public void onPressBackToTournamentView() throws IOException {
    TableTennisApplication.changeScene("tournament-view.fxml");
  }


  /**
   * Adds players with the names given in the textfields to a register handled in the BracketviewController
   * Also clears the register before it adds as an extra security step
   */
  public void onPressAdd() {
    if (textFieldPlayer1.getText().isBlank() || textFieldPlayer2.getText().isBlank()) {
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setContentText("Please enter a name into the text field before proceeding!");
      alert.setTitle("WARNING");
      alert.showAndWait();
    }
    else {
      try {
        TournamentController.tournamentRegister.clear();
        TournamentController.tournamentRegister.addPlayer(new Player(textFieldPlayer1.getText()));
        TournamentController.tournamentRegister.addPlayer(new Player(textFieldPlayer2.getText()));
        TournamentController.tournamentRegister.addPlayer(new Player(textFieldPlayer3.getText()));
        TournamentController.tournamentRegister.addPlayer(new Player(textFieldPlayer4.getText()));
        playersAddedMessage.setVisible(true);
      } catch (Exception e) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setContentText("You need to use letters and only letters!");
        alert.setTitle("UNABLE TO CREATE BRACKET");
        alert.showAndWait();
      }
    }
  }

  /**
   * Loads from the NEWTOURNAMENT.csv file, proceeds to the first game.
   */
  public void onPressLoad() {
    try {
      TournamentController.tournamentRegister.clear();
      TournamentController.tournamentRegister.addPlayers(
          FileHandler.readCsv(Path.of("NEWTOURNAMENT.csv")));
      TableTennisApplication.changeScene("tournament-match-view.fxml");

    } catch (Exception e) {
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setContentText("No save found!");
      alert.setTitle("UNABLE TO CREATE BRACKET");
      alert.showAndWait();
    }
  }

  /**
   * Proceeds to the match.
   *
   * @throws IOException If the file is not found
   */
  public void onPressPlay() throws IOException {
    if (TournamentController.tournamentRegister.getParticipants().size() <= 2) {
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setContentText("No players are added!");
      alert.setTitle("UNABLE TO CREATE BRACKET");
      alert.showAndWait();
    } else {
      TableTennisApplication.changeScene("create-tournament-match.fxml");
    }
  }
}
