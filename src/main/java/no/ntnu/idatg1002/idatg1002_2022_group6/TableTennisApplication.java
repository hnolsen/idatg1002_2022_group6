package no.ntnu.idatg1002.idatg1002_2022_group6;

import java.io.IOException;
import java.util.Objects;
import java.util.Random;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Represents the table tennis application.
 *
 * @author Group6
 * @version 1.0
 */
public class TableTennisApplication extends Application {

  private static      Stage  stage;
  public static final Random random = new Random();

  /**
   * This launches the application.
   * @param args The arguments passed to the application.
   */
  public static void main(String[] args) {
    launch();
  }

  /**
   * Sets the stage of the application.
   *
   * @param stage The main stage of the application.
   * @throws IOException if the fxml file cannot be found.
   */
  @Override
  public void start(Stage stage) throws IOException {
    TableTennisApplication.stage = stage;
    stage.setResizable(false);
    Parent root = FXMLLoader.load(
        Objects.requireNonNull(getClass().getResource("table-tennis-view.fxml")));
    stage.setTitle("Table Tennis");
    stage.setScene(new Scene(root, 800, 600));
    stage.show();
  }

  /**
   * Changes the scene shown to the user to a given scene.
   *
   * @param fxml The name of the fxml file to be loaded.
   * @throws IOException if the fxml file cannot be found.
   */
  public static void changeScene(String fxml) throws IOException {
    Parent pane = FXMLLoader.load(
        Objects.requireNonNull(TableTennisApplication.class.getResource(fxml)));
    stage.getScene().setRoot(pane);
  }
}