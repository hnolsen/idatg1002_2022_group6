package no.ntnu.idatg1002.idatg1002_2022_group6;

import java.io.IOException;

/**
 * Represents the controller for the info menu.
 *
 * @author Group6
 * @version 1.0
 */
public class InfoMenuController {

  /**
   * Button: returns to menu on click
   * @throws IOException IOException
   */

  public void onPressBackToMenu() throws IOException {
    TableTennisApplication tableTennisApplication = new TableTennisApplication();
    tableTennisApplication.changeScene("table-tennis-view.fxml");
  }
}
