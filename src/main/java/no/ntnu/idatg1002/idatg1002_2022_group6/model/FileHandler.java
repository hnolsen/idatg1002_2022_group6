package no.ntnu.idatg1002.idatg1002_2022_group6.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * FileHandler is a class that handles the reading and writing of files.
 *
 * @author Group 6
 * @version 1.0.0
 */
public class FileHandler {

  /**
   * Write players to a csv file.
   *
   * @param playerList The list of players to write to the file.
   * @param filename   The name / path of the file to write to.
   */
  public static void writeCsv(List<Player> playerList, String filename) {
    try  {
      FileWriter file = new FileWriter(filename + ".csv");
      BufferedWriter buffer = new BufferedWriter(file);
      for (Player player : playerList) {
        buffer.write(player.toString());
        buffer.write("\n");
      }
      buffer.close();
      }
    catch (IOException e){
      System.err.println(e);
    }
  }

  /**
   * Read a csv file of player names and return a list of the players with the name in the file.
   *
   * @Param Path of the file
   * @return A list of players read from the file.
   */
  public static List<Player> readCsv(Path path) {
    List<Player> players = new ArrayList<>();
    try (BufferedReader reader = Files.newBufferedReader(path)) {
      String textLine;
      while ((textLine = reader.readLine()) != null) {
        String[] names = textLine.split(",");
        players.add(new Player(names[0]));
      }
    } catch (IOException e) {
      System.err.println(e.getMessage());
    }
    return players;
  }
}
