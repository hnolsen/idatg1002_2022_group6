package no.ntnu.idatg1002.idatg1002_2022_group6.model;

import java.util.Objects;
import java.util.UUID;

/**
 * Class representing a player. A player has a name and a score.
 *
 * @author Group6
 * @version 1.3.6
 */
public class Player {

  private final String name;
  private final int    playerId;

  /**
   * Constructor for the Player class. The name should match the regex pattern to be a valid name.
   * No special characters besides dash or hyphen, any of the Norwegian letters are allowed, and
   * the name should be between 2 and 15 characters long. Also, no leading, trailing or double
   * whitespaces are allowed.
   * The score should be a positive integer in the range 0-11. If it is not the constructor will
   * throw an IllegalArgumentException.
   *
   * @param name the name of the player as String.
   */
  public Player(String name) throws IllegalArgumentException {
    if (! name.matches("^(?!.* {2})(?=\\S)(?=.*\\S$)[a-zA-Z æÆøØåÅ'-]{2,15}$")) {
      throw new IllegalArgumentException(
          "The name cannot contain any leading, trailing or double whitespaces." +
              "It should be between 2 and 15 characters long, and not contain any special " +
              "characters beside a dash or a hyphen.");
    }
    this.name = name;
    this.playerId = generateUniqueId();
  }

  /**
   * Getter for the name of the player.
   *
   * @return the name of the player as String.
   */
  public String getName() {
    return name;
  }

  /**
   * Getter for the id of the player.
   */
  public int getPlayerId() {
    return playerId;
  }

  /**
   * A method for returning the details of a player as String.
   *
   * @return the players name as a String.
   */
  @Override
  public String toString() {
    return getName();
  }

  /**
   * Generates a random id for the player. Uses the universal unique identifier (UUID) class to
   * generate the id.
   *
   * @return a random id for the player.
   */
  private int generateUniqueId() {
    UUID uuid = UUID.randomUUID();
    return uuid.hashCode();
  }

  /**
   * A method for comparing two players. The players are equal if their names and ids are equal.
   *
   * @param o the object to compare to.
   * @return true if the object is a player and the name of the players is the same.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Player player = (Player) o;
    return getPlayerId() == player.getPlayerId() && getName().equals(player.getName());
  }

  /**
   * A method for returning the hashcode of the player.
   *
   * @return the hashcode of the player.
   */
  @Override
  public int hashCode() {
    return Objects.hash(getName(), getPlayerId());
  }
}
