package no.ntnu.idatg1002.idatg1002_2022_group6.model;

import java.util.List;

/**
 * Represents a match.
 *
 * @author Group6
 * @version 1.2.3
 */
public class Match {

  private final PlayerRegister playerRegister;
  private int scorePlayerOne;
  private int scorePlayerTwo;

  /**
   * Simplified constructor for Match.
   */
  public Match() {
    playerRegister = new PlayerRegister();
  }

  /**
   * This constructor can be used to register a match after it's been settled. Takes in a list of
   * the players in the match, and their scores.
   * @param players The list of players in the match.
   * @param scorePlayerOne The score of player one.
   * @param scorePlayerTwo The score of player two.
   */
  public Match(List<Player> players, int scorePlayerOne, int scorePlayerTwo) {
    this.playerRegister = new PlayerRegister(players);
    this.scorePlayerOne = scorePlayerOne;
    this.scorePlayerTwo = scorePlayerTwo;
  }

  public int incrementScorePlayerOne() {
    return ++scorePlayerOne;
  }

  public int incrementScorePlayerTwo() {
    return ++scorePlayerTwo;
  }

  /**
   * The play function, sets up a match between two random opponents and can be played out,
   * first to 11 wins.
   *
   * @return The first player to reach 11, the winner. Returns the player
   */
  /*

  public Player play(PlayerRegister input) {
    //var randomGen = input.getRandomNumGen();
    List<Player> theList = input.getPlayerList();
    //Player player1 = theList.get(randomGen.nextInt());
    Player player1 = theList.get(0);
    //Player player2 = theList.get(randomGen.nextInt());
    Player player2 = theList.get(1);

    //TODO: This should work now but TEST IT
    //TODO: Add the rule that you have to win by two points
    //Note to reader: This is dumbed down for the sake of the mvp, it was requested to do this
    //while (player1.getScore() < 11 || player2.getScore() < 11) {
      //player2.setScore(7);
      //player1.setScore(11);
      if (scorePlayer1()) {
        player1.setScore(player1.getScore() + 1);
      }
      if (scorePLayer2()) {
        player2.setScore(player2.getScore() + 1);
      }

    if (player1.getScore() >= 11) {
      player1.setScore(0);
      player2.setScore(0);
      return player1;
    }
    else {
      player1.setScore(0);
      player2.setScore(0);
      return player2;
    }
  }

   */

  /**
   * Used for the button to increment score upward
   *
   * @return a boolean that allows the play function to increment the score of player1 by 1 point
   */
  public boolean scorePlayer1() {
    return true;
  }

  /**
   * Used for the button to increment score upward
   *
   * @return a boolean that allows the play function to increment the score of player2 by 1 point
   */
  public boolean scorePLayer2() {
    return true;
  }

}
