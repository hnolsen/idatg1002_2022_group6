package no.ntnu.idatg1002.idatg1002_2022_group6.model;

import java.util.ArrayList;
import java.util.List;
import no.ntnu.idatg1002.idatg1002_2022_group6.TableTennisApplication;

/**
 * Represents a register of the players in a table tennis tournament.
 *
 * @author Group6
 * @version 1.3.4
 */
public class PlayerRegister {

  /**
   * Represents a list of players
   */
  private final List<Player> participants;

  /**
   * Simplified constructor for a player register.
   */
  public PlayerRegister() {
    participants = new ArrayList<>();
  }

  /**
   * Constructor for a player register that takes a list of players as input.
   */
  public PlayerRegister(List<Player> participants) {
    this.participants = participants;
  }

  /**
   * Gets the list of tournament participants.
   * @return The list of tournament participants.
   */
  public List<Player> getParticipants() {
    return participants;
  }

  /**
   * Gets a player at a given index in the list of players. Makes sure that the index is within the
   * bounds of the list, and that the list is not empty.
   */
  public Player getPlayer(int i) {
    if (participants.isEmpty()) {
      return null;
    }
    if (i < 0 || i >= participants.size()) {
      return null;
    }
    return participants.get(i);
  }

  /**
   * Gets a player with a given name from the list of players.
   */
  public Player getPlayer(String name) {
    return participants.stream().filter(p -> p.getName().equals(name)).findFirst().orElse(null);
  }


  /**
   * Adds a player to the list of players in the tournament. Checks if the player is already in the
   * list by comparing the player's id. That way the list can contain players with the same name,
   * as long as their id is different.
   *
   * @param player The player to be added.
   * @throws IllegalArgumentException If the player was already in the list.
   */

  public void addPlayer(Player player) throws IllegalArgumentException {
    if (participants.stream().anyMatch(p -> p.getPlayerId() == player.getPlayerId())) {
      throw new IllegalArgumentException("Player with id " + player.getName() + " already exists");
    }
    participants.add(player);
  }

  /**
   * Adds a list of players to the list of players in the tournament, using the addPlayer() method.
   *
   * @see #addPlayer(Player) for the method that adds a player to the list.
   */
  public void addPlayers(List<Player> players) {
    for (Player player : players) {
      addPlayer(player);
    }
  }

  /**
   * Removes a player from the list of players if it matches a given player.
   *
   * @param playerToRemove The player to be removed.
   */

  public void removePlayer(Player playerToRemove) {
    participants.removeIf(p -> p.equals(playerToRemove));
  }

  /**
   * Gets a given number of random players as a list.
   *
   * @param playersToAdd The number of players to add.
   * @return A list of a given number of random players.
   */
  public List<Player> getRandomPlayers(int playersToAdd) {
    List<Player> randPlayersList = new ArrayList<>();
    int randNum = TableTennisApplication.random.nextInt(1, participants.size());

    // If the player list is not empty, adds a given number of players to the random player list.
    if (! participants.isEmpty()) {
      for (int i = 1; i <= playersToAdd; i++) {
        randPlayersList.add(participants.get(randNum));
      }
    }
    return randPlayersList;
  }

  /**
   * Clears the list of tournament participants.
   */
  public void clear() {
    participants.clear();
  }
}
