package no.ntnu.idatg1002.idatg1002_2022_group6;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The create tournament match controller.
 */
public class CreateTournamentMatchController implements Initializable {
    @FXML
    private Label player1Label;
    @FXML
    private Label player2Label;


    /**
     * Returns to the tournament view
     * @throws IOException If the scene could not be loaded.
     */
    public void onPressBackInTournamentMatchView() throws IOException {
        TableTennisApplication.changeScene("tournament-view.fxml");
    }

    /**
     * Switches scene to the tournament match screen.
     * @throws IOException If the scene could not be loaded.
     */
    public void onPressPlayMatch() throws IOException {
        TableTennisApplication.changeScene("tournament-match-view.fxml");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //We dont really needs this as of right now
    }


}
